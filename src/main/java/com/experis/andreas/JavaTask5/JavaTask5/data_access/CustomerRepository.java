package com.experis.andreas.JavaTask5.JavaTask5.data_access;

import com.experis.andreas.JavaTask5.JavaTask5.models.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {
    // Setting up the connection object
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    /**
     * Gets all the customers from the database
     * @return ArrayList of CustomerRest
     */
    public ArrayList<CustomerRest> getAllCustomers(){
        //This uses the CustomerRest to return to the user. This is so the user don't get 'null' on every field that's not supposed to be returned.
        ArrayList<CustomerRest> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone FROM Customer");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                CustomerRest newCustomer = new CustomerRest();
                newCustomer.setCustomerId(result.getInt("CustomerId"));
                newCustomer.setFirstName(result.getString("FirstName"));
                newCustomer.setLastName(result.getString("LastName"));
                newCustomer.setCountry(result.getString("Country"));
                newCustomer.setPostalCode(result.getString("PostalCode"));
                newCustomer.setPhone(result.getString("Phone"));
                customers.add(newCustomer);
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return customers;
    }

    /**
     * Gets a specific customer
     * @param customerId The id of the customer to be returned
     * @return A Customer object
     */
    public Customer getSpecificCustomer(String customerId){
        Customer customer = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM Customer WHERE CustomerId = ?");
            prep.setString(1, customerId);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                customer = new Customer(
                        result.getInt("CustomerId"),
                        result.getString("FirstName"),
                        result.getString("LastName"),
                        result.getString("Company"),
                        result.getString("Address"),
                        result.getString("City"),
                        result.getString("State"),
                        result.getString("Country"),
                        result.getString("PostalCode"),
                        result.getString("Phone"),
                        result.getString("Fax"),
                        result.getString("Email"),
                        result.getInt("SupportRepId")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return customer;
    }

    /**
     * Post a new customer to the database
     * @param customer The customer to post
     * @return Pair of Boolean (success) and Customer (The customer object that was added)
     */
    public Pair<Boolean, Customer> addCustomer(Customer customer){
        Boolean success = false;

        try {

            //Getting the amount of employee

            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO " +
                            "Customer(CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId) " +
                            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);");
            prep.setInt(1,customer.getCustomerId());
            prep.setString(2,customer.getFirstName());
            prep.setString(3,customer.getLastName());
            prep.setString(4,customer.getCompany());
            prep.setString(5,customer.getAddress());
            prep.setString(6,customer.getCity());
            prep.setString(7,customer.getState());
            prep.setString(8,customer.getCountry());
            prep.setString(9,customer.getPostalCode());
            prep.setString(10,customer.getPhone());
            prep.setString(11,customer.getFax());
            prep.setString(12,customer.getEmail());
            prep.setInt(13,10);

            int result = prep.executeUpdate();
            success = (result != 0);

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(false, null);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, customer);
    }

    /**
     * Put a customer to the database
     * @param customer The customer to put
     * @return Pair of Boolean (success) and Customer (The customer object that was updated)
     */
    public Pair<Boolean, Customer> updateCustomer(Customer customer){
        Boolean success = false;

        try {

            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE Customer " +
                            "SET CustomerId=?, FirstName=?, LastName=?, Company=?, Address=?, City=?, State=?, Country=?, PostalCode=?, Phone=?, Fax=?, Email=?, SupportRepId=? " +
                            "WHERE CustomerId=?");
            prep.setInt(1,customer.getCustomerId());
            prep.setString(2,customer.getFirstName());
            prep.setString(3,customer.getLastName());
            prep.setString(4,customer.getCompany());
            prep.setString(5,customer.getAddress());
            prep.setString(6,customer.getCity());
            prep.setString(7,customer.getState());
            prep.setString(8,customer.getCountry());
            prep.setString(9,customer.getPostalCode());
            prep.setString(10,customer.getPhone());
            prep.setString(11,customer.getFax());
            prep.setString(12,customer.getEmail());
            prep.setInt(13,customer.getSupportRepId());
            prep.setInt(14,customer.getCustomerId());

            int result = prep.executeUpdate();
            success = (result != 0);

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(false, null);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(true, customer);
    }


    // Number of customers in each country, sorted descending

    /**
     * Gets the number of customers in each country
     * @return ArrayList of CustomerInCountry
     */
    public ArrayList<CustomersInCountry> getCountryByDescendingCustomers(){
        ArrayList<CustomersInCountry> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "SELECT Country, COUNT(Country) as Amount " +
                    "FROM Customer " +
                    "GROUP BY Country " +
                    "ORDER BY Amount DESC");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                customers.add(new CustomersInCountry(
                        result.getString("Country"),
                        result.getInt("Amount")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return customers;
    }

    // Gets the highest spending customers

    /**
     * Gets the highest spending customers
     * @return ArrayList og Customer
     */
    public ArrayList<Customer> getCustomersHighestSpending(){
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "SELECT c.CustomerId, c.FirstName, c.LastName, c.Company, c.Address, c.City, c.State, c.Country, c.PostalCode, c.Phone, c.Fax, c.Email, c.SupportRepId, " +
                            "i.CustomerId, i.Total " +
                            "FROM Customer c " +
                            "JOIN Invoice i " +
                            "ON c.CustomerId = i.CustomerId " +
                            "ORDER BY i.Total DESC");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                for (int i = 1; i < result.getMetaData().getColumnCount() + 1; i++) {
                    System.out.print(" " + result.getMetaData().getColumnName(i) + "=" + result.getObject(i));
                }
                System.out.println("");
                customers.add(new Customer(
                        result.getInt("CustomerId"),
                        result.getString("FirstName"),
                        result.getString("LastName"),
                        result.getString("Company"),
                        result.getString("Address"),
                        result.getString("City"),
                        result.getString("State"),
                        result.getString("Country"),
                        result.getString("PostalCode"),
                        result.getString("Phone"),
                        result.getString("Fax"),
                        result.getString("Email"),
                        result.getInt("SupportRepId"),
                        result.getDouble("Total")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return customers;
    }

    // Gets a customers most popular genre

    /**
     * Gets a specific customers most popular genre
     * @param customerId The customers id
     * @return ArrayList of Genre
     */
    public ArrayList<Genre> getCustomersMostPopularGenre(String customerId){
        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            //Splitting the query into to seperate queries

            //The first gets the max amount of the count of genreid's for a given customer
            PreparedStatement maxAmount = conn.prepareStatement(""+
                    "SELECT MAX(Amount) " +
                    "FROM (" +
                        "SELECT COUNT(g.GenreId) Amount " +
                        "FROM Customer " +
                        "JOIN Invoice " +
                        "ON Customer.CustomerId = Invoice.CustomerId " +
                        "INNER JOIN InvoiceLine il " +
                        "ON Invoice.InvoiceId = il.InvoiceId " +
                        "INNER JOIN Track t " +
                        "ON il.TrackId = t.TrackId " +
                        "INNER JOIN Genre g " +
                        "ON t.GenreId = g.GenreId " +
                        "WHERE (Customer.CustomerId = ?) " +
                        "GROUP BY g.GenreId " +
                    ")");
            maxAmount.setString(1, customerId);
            ResultSet maxResult = maxAmount.executeQuery();
            int max = -1;

            while(maxResult.next()){
                max = maxResult.getInt("MAX(Amount)");
            }

            //The second one gets the actault genres based on the max and customerid
            PreparedStatement prep = conn.prepareStatement(
                    "SELECT c.CustomerId, t.GenreId, g.Name, COUNT(g.GenreID) as Amount " +
                            "FROM Customer c " +
                            "JOIN Invoice i " +
                            "ON c.CustomerId = i.CustomerId " +
                            "INNER JOIN InvoiceLine il " +
                            "ON i.InvoiceId = il.InvoiceId " +
                            "INNER JOIN Track t " +
                            "ON il.TrackId = t.TrackId " +
                            "INNER JOIN Genre g " +
                            "ON t.GenreId = g.GenreId " +
                            "WHERE (c.CustomerId = ?) " +
                            "GROUP BY g.GenreId " +
                            "HAVING Amount = ? " +
                            "ORDER BY Amount DESC");

            prep.setString(1, customerId);
            prep.setInt(2, max);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                //Code to print out in terminal instead of making object
                /*for (int i = 1; i < result.getMetaData().getColumnCount() + 1; i++) {
                    System.out.print(" " + result.getMetaData().getColumnName(i) + "=" + result.getObject(i));
                }
                System.out.println("");*/
                genres.add(new Genre(
                        result.getInt("GenreId"),
                        result.getString("Name")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        return genres;
    }


}
