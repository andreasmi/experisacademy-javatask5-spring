package com.experis.andreas.JavaTask5.JavaTask5.models;

/**
 * This is a model class for displaying the amount of customers in a country
 */
public class CustomersInCountry {
    String countryName;
    int amountOfCustomers;

    // Constructor

    public CustomersInCountry(String countryName, int amountOfCustomers) {
        this.countryName = countryName;
        this.amountOfCustomers = amountOfCustomers;
    }

    // Getters and setters

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getAmountOfCustomers() {
        return amountOfCustomers;
    }

    public void setAmountOfCustomers(int amountOfCustomers) {
        this.amountOfCustomers = amountOfCustomers;
    }
}
