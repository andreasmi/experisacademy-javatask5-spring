package com.experis.andreas.JavaTask5.JavaTask5.models;

public class Pair<T, U> {
    public final T t;
    public final U u;

    public Pair(T t, U u){
        this.t = t;
        this.u = u;
    }
}
