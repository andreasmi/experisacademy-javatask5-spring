package com.experis.andreas.JavaTask5.JavaTask5.data_access;

import com.experis.andreas.JavaTask5.JavaTask5.models.Artist;
import com.experis.andreas.JavaTask5.JavaTask5.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;

public class ArtistRepository {
    // Setting up the connection object
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    /**
     * Gets all the artist from the database
     * @return ArrayList of Artist
     */
    public ArrayList<Artist> getAllArtists(){
        ArrayList<Artist> artists = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM Artist");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                artists.add(new Artist(
                        result.getInt("ArtistId"),
                        result.getString("Name")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return artists;
    }

    //Get random tracks

    /**
     * Gets random artist from the database
     * @param amountOfArtists The amount of random artist to return
     * @return A ArrayList of Artist
     */
    public ArrayList<Artist> getRandomArtists(int amountOfArtists){
        //Gets all the tracks
        ArrayList<Artist> allArtists = getAllArtists();
        ArrayList<Artist> randomArtists = new ArrayList<>();

        //Random tracks variables
        int totalArtists;
        Random rand = new Random();
        int randomNumber;

        for(int i = 0; i < amountOfArtists; i++){
            totalArtists = allArtists.size();
            randomNumber = rand.nextInt(totalArtists);
            randomArtists.add(allArtists.get(randomNumber));
            allArtists.remove(randomNumber);
        }

        return randomArtists;
    }
}
