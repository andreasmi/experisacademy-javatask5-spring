package com.experis.andreas.JavaTask5.JavaTask5.controllers;

import com.experis.andreas.JavaTask5.JavaTask5.data_access.ArtistRepository;
import com.experis.andreas.JavaTask5.JavaTask5.data_access.TrackRepository;
import com.experis.andreas.JavaTask5.JavaTask5.models.Track;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@Controller
public class RouterController {

    TrackRepository trackRepository = new TrackRepository();
    ArtistRepository artistRepository = new ArtistRepository();

    /**
     * Thymeleaf endpoint for index page
     * @param model The thymeleaf model
     * @return The name of the template file
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("search", "");
        model.addAttribute("artists", artistRepository.getRandomArtists(5));
        model.addAttribute("tracks", trackRepository.getRandomTracks(5));
        model.addAttribute("genres", trackRepository.getRandomGenres(5));
        return "index";
    }

    /**
     * Endpoint for when someone manually goes to the search page
     * @param model The thymeleaf model
     * @return The name of the template file
     */
    @RequestMapping(value = "/searchTrack", method = RequestMethod.GET)
    public String searchTrack(Model model){
        ArrayList<Track> tracks = trackRepository.getRandomTracks(10);
        model.addAttribute("success", false);

        for(Track t : tracks){
            System.out.println(tracks.indexOf(t));
        }
        model.addAttribute("tracks", tracks);
        return "search-tracks";
    }

    /**
     * Endpoint for the search page
     * @param searchText The search parameter
     * @param http HttpServletResponse for the possibility to redirect
     * @param model The thymeleaf model
     * @return The name of the template file
     */
    @RequestMapping(value = "/searchTrack", method = RequestMethod.POST)
    public String searchTrack(@RequestParam String searchText, HttpServletResponse http, Model model){
        //If the user searched for nothing
        if(searchText.trim().equals("")){
            try {
                http.sendRedirect("/");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "index";
        }
        ArrayList<Track> tracks = trackRepository.searchTracks(searchText);
        model.addAttribute("tracks", tracks);
        model.addAttribute("success", tracks.size() > 0);
        return "search-tracks";
    }

}
