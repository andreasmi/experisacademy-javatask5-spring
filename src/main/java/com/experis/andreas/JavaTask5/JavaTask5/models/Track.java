package com.experis.andreas.JavaTask5.JavaTask5.models;

/**
 * This is the model class for the track table in the database
 */
public class Track {
    int TrackId;
    String Name;
    int AlbumId;
    String MediaTypeId;
    int GenreId;
    String Composer;
    String Milliseconds;
    String Bytes;
    String UnitPrice;
    String albumName;
    String genreName;

    //Constructors

    public Track() {
    }

    public Track(int trackId, String name, int albumId, String mediaTypeId, int genreId, String composer, String milliseconds, String bytes, String unitPrice) {
        TrackId = trackId;
        Name = name;
        AlbumId = albumId;
        MediaTypeId = mediaTypeId;
        GenreId = genreId;
        Composer = composer;
        Milliseconds = milliseconds;
        Bytes = bytes;
        UnitPrice = unitPrice;
    }

    // Getters and Setters

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getGenre(){
        return genreName;
    }

    public void setGenre(String genreName){
        this.genreName = genreName;
    }

    public int getTrackId() {
        return TrackId;
    }

    public void setTrackId(int trackId) {
        TrackId = trackId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(int albumId) {
        AlbumId = albumId;
    }

    public String getMediaTypeId() {
        return MediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {
        MediaTypeId = mediaTypeId;
    }

    public int getGenreId() {
        return GenreId;
    }

    public void setGenreId(int genreId) {
        GenreId = genreId;
    }

    public String getComposer() {
        return Composer;
    }

    public void setComposer(String composer) {
        Composer = composer;
    }

    public String getMilliseconds() {
        return Milliseconds;
    }

    public void setMilliseconds(String milliseconds) {
        Milliseconds = milliseconds;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String bytes) {
        Bytes = bytes;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }
}
