package com.experis.andreas.JavaTask5.JavaTask5.models;

/**
 *  This is the model class for Genre
 */
public class Genre {
    private int genreId;
    private String genreName;

    //Constructor


    public Genre(int genreId, String genreName) {
        this.genreId = genreId;
        this.genreName = genreName;
    }

    //Getters and setters

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
