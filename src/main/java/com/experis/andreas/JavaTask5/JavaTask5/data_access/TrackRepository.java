package com.experis.andreas.JavaTask5.JavaTask5.data_access;

import com.experis.andreas.JavaTask5.JavaTask5.models.Genre;
import com.experis.andreas.JavaTask5.JavaTask5.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class is a repository for the Tracks in the database
 */
public class TrackRepository {
    // Setting up the connection object
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    /**
     * This gets all of the Tracks from the Track table in the database
     * @return ArrayList of Tracks from the database
     */
    public ArrayList<Track> getAllTracks(){
        ArrayList<Track> tracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM Track");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                tracks.add(new Track(
                        result.getInt("TrackId"),
                        result.getString("Name"),
                        result.getInt("AlbumId"),
                        result.getString("MediaTypeId"),
                        result.getInt("GenreId"),
                        result.getString("Composer"),
                        result.getString("Milliseconds"),
                        result.getString("Bytes"),
                        result.getString("UnitPrice")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return tracks;
    }

    /**
     * This gets a specific track from the database
     * @param trackId The id of the tracks that will be returned
     * @return A Track
     */
    public Track getSpecificTrack(int trackId){
        Track track = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM Track WHERE TrackId = ?");
            prep.setInt(1, trackId);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                track = new Track(
                        result.getInt("TrackId"),
                        result.getString("Name"),
                        result.getInt("AlbumId"),
                        result.getString("MediaTypeId"),
                        result.getInt("GenreId"),
                        result.getString("Composer"),
                        result.getString("Milliseconds"),
                        result.getString("Bytes"),
                        result.getString("UnitPrice")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return track;
    }



    //Search tracks

    /**
     * This method is for searching through the Track database based on the name.
     * It uses several different methods for accomplishing it's means.
     * @param name The search parameter
     * @return An ArrayList of Tracks that match the search
     */
    public ArrayList<Track> searchTracks(String name){
        //The inner function gets the search result. The middle and outre function populates the list with Gerne- and Album- names.
        //It does not matter which order the two outer most function is called
        return populateListWithAlbum(populateListWithGenre(getFullSearchResults(name)));
    }

    //Populate with genre

    /**
     * Populates a list of Tracks with their Genre name
     * @param tracks The track list
     * @return The same ArrayList but every track has now a genre name
     */
    public ArrayList<Track> populateListWithGenre(ArrayList<Track> tracks){
        ArrayList<Track> results = tracks;
        try {
            conn = DriverManager.getConnection(URL);
            for(Track t : results){
                PreparedStatement prep = conn.prepareStatement("SELECT Name FROM Genre WHERE GenreId = ?");
                prep.setInt(1, t.getGenreId());
                ResultSet result = prep.executeQuery();

                while(result.next()){
                    t.setGenre(result.getString("Name"));
                }
            }


        } catch (Exception e){
            System.out.println(e.toString());
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        return results;
    }


    //Populate with album

    /**
     * Populates a list of Tracks with their Album name
     * @param tracks The track list
     * @return The same ArrayList but every track has now a album name
     */
    public ArrayList<Track> populateListWithAlbum(ArrayList<Track> tracks){
        ArrayList<Track> results = tracks;
        try {
            conn = DriverManager.getConnection(URL);
            for(Track t : results){
                PreparedStatement prep = conn.prepareStatement("SELECT Title FROM Album WHERE AlbumId = ?");
                prep.setInt(1, t.getAlbumId());
                ResultSet result = prep.executeQuery();

                while(result.next()){
                    t.setAlbumName(result.getString("Title"));
                }
            }


        } catch (Exception e){
            System.out.println(e.toString());
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        return results;
    }


    //Search tracks

    /**
     * Search the databases track table. The search parameter could be anywhere in the tracks name
     * @param name The search parameter
     * @return A ArrayList of tracks that matches the search parameter
     */
    public ArrayList<Track> getFullSearchResults(String name){
        ArrayList<Track> tracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM Track WHERE Name LIKE ?");
            //The % is for wildcard. By putting a '%' before and after the search parameter, the parameter could be anywhere in the songs name
            prep.setString(1, "%"+name+"%");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                tracks.add(new Track(
                        result.getInt("TrackId"),
                        result.getString("Name"),
                        result.getInt("AlbumId"),
                        result.getString("MediaTypeId"),
                        result.getInt("GenreId"),
                        result.getString("Composer"),
                        result.getString("Milliseconds"),
                        result.getString("Bytes"),
                        result.getString("UnitPrice")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        return tracks;
    }


    //Get random tracks

    /**
     * Gets random tracks from the database
     * @param amountOfTracks The amount of random tracks to return
     * @return A ArrayList of Tracks
     */
    public ArrayList<Track> getRandomTracks(int amountOfTracks){
        //Gets all the tracks
        ArrayList<Track> allTracks = getAllTracks();
        //Creating the returning ArrayList
        ArrayList<Track> randomTracks = new ArrayList<>();

        //Random tracks variables
        int totalTracks;
        Random rand = new Random();
        int randomNumber;

        //Getting a random track, adding it to the randomTrack array and removing it from the allTrack array
        for(int i = 0; i < amountOfTracks; i++){
            totalTracks = allTracks.size();
            randomNumber = rand.nextInt(totalTracks);
            randomTracks.add(allTracks.get(randomNumber));
            allTracks.remove(randomNumber);
        }

        return randomTracks;
    }

    //Get all genres

    /**
     * Gets all the genres
     * @return ArrayList of Genre
     */
    public ArrayList<Genre> getAllGenres(){
        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM Genre");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                genres.add(new Genre(
                        result.getInt("GenreId"),
                        result.getString("Name")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return genres;
    }

    //Get random genres

    /**
     * Gets random genre from the database
     * @param amountOfGenres The amount of random genres to return
     * @return A ArrayList of Genre
     */
    public ArrayList<Genre> getRandomGenres(int amountOfGenres){
        //Gets all the genres
        ArrayList<Genre> allGenres = getAllGenres();
        ArrayList<Genre> randomGenres = new ArrayList<>();

        //Random genres variables
        int totalGenres;
        Random rand = new Random();
        int randomNumber;

        //Getting a random track, adding it to the randomTrack array and removing it from the allTrack array
        for(int i = 0; i < amountOfGenres; i++){
            totalGenres = allGenres.size();
            randomNumber = rand.nextInt(totalGenres);
            randomGenres.add(allGenres.get(randomNumber));
            allGenres.remove(randomNumber);
        }

        return randomGenres;
    }
}
