package com.experis.andreas.JavaTask5.JavaTask5.api;

import com.experis.andreas.JavaTask5.JavaTask5.data_access.CustomerRepository;
import com.experis.andreas.JavaTask5.JavaTask5.models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    CustomerRepository customerRepository = new CustomerRepository();

    /**
     * Endpoint for getting all the customers
     * @return ArrayList of CustomerRest
     */
    @RequestMapping(value = "/api/v1/customers", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<CustomerRest>> getAllCustomer(){
        ArrayList<CustomerRest> result = customerRepository.getAllCustomers();

        if(result == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Endpoint for getting a specific customer
     * @param customerId The id of the customer to get
     * @return A Customer
     */
    @RequestMapping(value = "/api/v1/customers/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<Customer>  getSpecificCustomer(@PathVariable String customerId){
        Customer result = customerRepository.getSpecificCustomer(customerId);

        if(result == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Endpoint for posting a new customer
     * @param customer The customer to post
     * @return The posted customer
     */
    @RequestMapping(value = "/api/v1/customers", method = RequestMethod.POST)
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer){
        Pair<Boolean, Customer> result = customerRepository.addCustomer(customer);

        if(!result.t){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(result.u, HttpStatus.CREATED);
    }

    /**
     * Endpoint for putting a new customer
     * @param customer The customer to update
     * @return The updated customer
     */
    @RequestMapping(value = "/api/v1/customers", method = RequestMethod.PUT)
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer){
        Pair<Boolean, Customer> result = customerRepository.updateCustomer(customer);

        if(!result.t){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(result.u, HttpStatus.CREATED);
    }

    /**
     * Endpoint for getting the amount of customers in each country
     * @return ArrayList of CustomerInCountry
     */
    @RequestMapping(value = "/api/v1/customers/in/country", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<CustomersInCountry>> getCountryByDescendingCustomers(){
        ArrayList<CustomersInCountry> result = customerRepository.getCountryByDescendingCustomers();
        if(result == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Endpoint for getting the highest spenders
     * @return ArrayList of Customer
     */
    @RequestMapping(value = "/api/v1/customers/spending/highest", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Customer>> getCustomersHighestSpending(){
        ArrayList<Customer> result = customerRepository.getCustomersHighestSpending();
        if(result == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Endpoint for getting a specific customers most popular genre
     * @param customerId The customers id
     * @return ArrayList of Genre
     */
    @RequestMapping(value = "/api/v1/customers/{customerId}/popular/genre")
    public ResponseEntity<ArrayList<Genre>> getCustomersMostPopularGenre(@PathVariable String customerId){
        ArrayList<Genre> result = customerRepository.getCustomersMostPopularGenre(customerId);
        if(result == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
