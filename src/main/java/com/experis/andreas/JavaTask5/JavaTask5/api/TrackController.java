package com.experis.andreas.JavaTask5.JavaTask5.api;

import com.experis.andreas.JavaTask5.JavaTask5.data_access.TrackRepository;
import com.experis.andreas.JavaTask5.JavaTask5.models.Track;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class TrackController {
    TrackRepository trackRepository = new TrackRepository();

    /**
     * Endpoint for getting all tracks
     * @return ArrayList of Track
     */
    @RequestMapping(value = "/api/v1/tracks", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Track>> getAllTracks(){
        ArrayList<Track> result = trackRepository.getAllTracks();

        if(result == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    /**
     * Endpoint for getting specific Track
     * @param trackId The id of the track to be returned
     * @return A Track
     */
    @RequestMapping(value = "/api/v1/tracks/{trackId}", method = RequestMethod.GET)
    public ResponseEntity<Track> getSpecificTrack(@PathVariable int trackId){
        Track result = trackRepository.getSpecificTrack(trackId);

        if(result == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
