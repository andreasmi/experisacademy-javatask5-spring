FROM openjdk:14
ADD target/JavaTask5.jar JavaTask5.jar
ENTRYPOINT [ "java", "-jar", "/JavaTask5.jar" ]