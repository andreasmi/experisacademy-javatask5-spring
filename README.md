# Experis Academy Java Task5: Spring
This is an assigment for the Java course at Noroff Accelerate. The task was to make a spring project with endpoints for both Thymeleaf and api endpoints that queried a SQLite database that's locally stored.


## Usage
The application is hosted on Heroku (https://experis-academy-java.herokuapp.com/). There is also other api calls which is described in the JavaTask5.postman_collection.json file in the "Doc" directory. There is a docker version (https://experis-academy-java-docker.herokuapp.com/), but it's the exact same as the other.

## Features
The main part of the application is to search for a song from the database. The database contains different tables, some of them are Tracks, Customers and Invoices. The other part of the application is the api endpoints. Here you can do CRUD operations on the Customer table, get the countries with the most customers and get a customers most popular genre.